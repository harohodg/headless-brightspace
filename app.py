import requests, getpass 
import d2lvalence.auth as d2lauth
from config import app_config

user = input('BrightSpace Username : ')
pwd  = getpass.getpass('BrightSpace Password : ') 

trusted_url = app_config['trusted_url']
app_context = d2lauth.fashion_app_context(app_id=app_config['app_id'], app_key=app_config['app_key'])
aurl        = app_context.create_url_for_authentication(app_config["lms_host"], trusted_url)

headers = {'User-Agent': 'Mozilla/5.0'}
payload = {'target':aurl,
           'noredirect':'1',
           'loginPath': '/d2l/login',
           'userName' : user,
           'password' : pwd
           }

session = requests.Session()
r = session.post('https://{}/d2l/lp/auth/login/login.d2l'.format( app_config['lms_host'] ), headers=headers, data=payload)
r = session.get('https://{}/d2l/lp/auth/login/ProcessLoginActions.d2l'.format( app_config['lms_host'] ), allow_redirects=False )
r = session.get('https://{}/'.format( app_config['lms_host'] ) + r.headers['Location'], allow_redirects=False )

uc         = app_context.create_user_context( result_uri=r.headers['Location'], host=app_config['lms_host'], encrypt_requests=app_config['encrypt_requests'])

#Change the following code to call different routes
auth_route = uc.create_authenticated_url('/d2l/api/lp/{}/users/whoami'.format(app_config['lms_ver']['lp']), method='GET')
r          = session.get(auth_route)
print(r.status_code, r.json() )

