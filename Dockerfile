############################################################
# Dockerfile to build docker image for testing Brightspace Python API 
# without a browser
# Based on Ubuntu
############################################################

# Set the base image to Ubuntu
FROM ubuntu

# File Author / Maintainer
MAINTAINER Harold Hodgins

# Update the sources list
RUN apt-get update


# Install Python and Basic Python Tools
RUN apt-get install -y python3 python-dev python-distribute python3-pip

# Get copy of repo with correct API credentials
ADD ./requirements.txt /app/

# Get pip to download and install requirements:
RUN pip3 install -r /app/requirements.txt

# Expose ports
EXPOSE 8080

# Set volume so we can live edit
VOLUME /app

# Set the default directory where CMD will execute
WORKDIR /app

# Set the default command to execute
# when creating a new container
CMD python3 app.py

