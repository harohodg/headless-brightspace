app_config = {
               'app_id': 'your app id',
               'app_key': 'your app key',
               'lms_host': 'your lms host (eg. wobmats.university.com)',
               'encrypt_requests': True,
               'lms_ver': {'lp':'1.20','le':'1.26','ep':'2.5'},
               'trusted_url': 'your trusted url including http(s)://.....',
               }
